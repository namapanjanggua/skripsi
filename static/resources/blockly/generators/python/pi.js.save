'use strict';

goog.require('Blockly.Python');

function add_parentheses(string){
   return '\"'+string+'\"';
}

function combine(strings){
   ret = '';
   for (i=0; i<strings.length; i++){
     ret = ret + strings[i] + "\n"
   }
   return ret;
}

function add_newlines(strings, flag){
   ret = '';
   for (i = 0; i < strings.length; i++){
	if (flag == 0)
	  ret = ret + add_parentheses(strings[i])+"+"+add_parentheses("\\n");
	else
	  ret = ret + strings[i]+"+"+add_parentheses("\\n");
	if (i != strings.length-1)
	  ret = ret + "+";
   }
   return ret;
}

function send_to_zmq(port){
  var send = [];
  send.push('def send_to_zmq(body):');
  send.push('\\tcontext = zmq.Context()');
  send.push('\\tsocket = context.socket(zmq.REQ)');
  send.push("\\tsocket.connect('tcp://localhost:"+port+"')");
  send.push('\\tsocket.send(body)');
  send.push('\\tmessage = socket.recv()');
  send.push("\\tprint('Received reply %s [ %s ]' % (body, message))");

  return add_newlines(send, 0);
}

function parseArrayToCommas(array){
  var ret = "";
  for(i=0; i<array.length; i++){
    ret = ret + array[i];
    if (i != array.length-1)
      ret = ret + ",";
  }
  return ret;
}

function callback(id_sensor){
  var send = [];
  send.push('def callback():');
  send.push("\\tprint('callback')");
  send.push('\\tbody = {}');
  send.push("\\tdevice_json = urllib2.urlopen('http://localhost:8000/devices/"+id_sensor+"/')");
  send.push('\\tdevice = json.load(device_json)');
  send.push("\\tbody['sender'] = str("+id_sensor+")");
  send.push("\\tbody['target'] = device['connected_devices']");
  send.push('\\tbody_json = json.dumps(body)');
  send.push('\\tsend_to_zmq(body_json)');
  send.push("\\tprint('sent')")

  return add_newlines(send, 0);
}

function sensor_action(id_sensor, port_gpio, port_zmq){
  var send = [];
  send.push(add_parentheses('import RPi.GPIO as GPIO'));
  send.push(add_parentheses('import zmq, time, sys, json, urllib2'));
  send.push(send_to_zmq(port_zmq));
  send.push(callback(id_sensor));
  send.push(add_parentheses('GPIO.setmode(GPIO.BCM)'));
  send.push(add_parentheses('GPIO.setwarnings(False)'));
  send.push(add_parentheses('GPIO.setup('+port_gpio+', GPIO.IN, pull_up_down=GPIO.PUD_UP)'));
  send.push(add_parentheses('while True:'));
  send.push(add_parentheses('\\tinput_state = GPIO.input('+port_gpio+')'));
  send.push(add_parentheses('\\tif input_state == GPIO.LOW:'));
  send.push(add_parentheses("\\t\\tprint('clicked')"));
  send.push(add_parentheses('\\t\\tcallback()'));
  send.push(add_parentheses('\\t\\twhile input_state == GPIO.LOW:'));
  send.push(add_parentheses('\\t\\t\\tinput_state = GPIO.input('+port_gpio+')'));
  send.push(add_parentheses("\\ttime.sleep(0.2)"));
  return add_newlines(send, 1);
}

function actuator_action(port_gpio, code){
  var send = [];
  send.push(add_parentheses('import RPi.GPIO as GPIO'));
  send.push(add_parentheses('import time'));
  send.push(add_parentheses('GPIO.setmode(GPIO.BCM)'));
  send.push(add_parentheses('GPIO.setwarnings(False)'));
  send.push(add_parentheses('channel = (int) ('+port_gpio+')'));
  send.push(add_parentheses('GPIO.setup(channel, GPIO.OUT)'));
  if (code == 0){ // turn on
    send.push(add_parentheses('GPIO.output('+port_gpio+', GPIO.HIGH)'));
  }else if (code == 1){ // turn off
    send.push(add_parentheses('GPIO.output('+port_gpio+', GPIO.LOW)'));
  }
  else if (code == 2){ //blink
    send.push(add_parentheses('GPIO.output('+port_gpio+', GPIO.HIGH)'));
    send.push(add_parentheses('time.sleep(1)'));
    send.push(add_parentheses('GPIO.output('+port_gpio+', GPIO.LOW)'));
  }
  else if (code == 3){ //disco
    send.push(add_parentheses('while True:'));
    send.push(add_parentheses('\\tGPIO.output('+port_gpio+', GPIO.HIGH)'));
    send.push(add_parentheses('\\ttime.sleep(0.5)'));
    send.push(add_parentheses('\\tGPIO.output('+port_gpio+', GPIO.LOW)'));
    send.push(add_parentheses('\\ttime.sleep(0.5)'));
  }
  return add_newlines(send, 1);
}

function getDeviceType(block){
  return JSON.parse(block.getFieldValue('port'))['device_type']['name'];
}

function getZMQPort(block){
  return JSON.parse(block.getFieldValue('port'))['port_zmq'];
}

function isLocal(block){
  return JSON.parse(block.getFieldValue('port'))['is_local'];
}

function gpio_in(channel){
  var channel_declaration = "gpio_pin="+channel;
  var set_mode = "GPIO.setmode(GPIO.BCM)";
  var setup = "GPIO.setup(gpio_pin, GPIO.IN)";
  return channel_declaration+"\n"+set_mode+"\n"+setup;
}

function gpio_out(channel){
  var channel_declaration = "gpio_pin="+channel;
  var set_mode = "GPIO.setmode(GPIO.BCM)";
  var setup = "GPIO.setup(gpio_pin, GPIO.OUT)"
  return channel_declaration+"\n"+set_mode+"\n"+setup;
}

function wait_until_pressed(channel){
  return 'while GPIO.input('+channel+') == False:\n time.sleep(0.2)';
}

function send_signal(channel, signal){
  if (signal == 1)
    return "GPIO.output("+channel+", GPIO.HIGH)";
  return "GPIO.output("+channel+", GPIO.LOW)";
}

Blockly.Python['when_btn'] = function(block) {
  var device_json = block.getFieldValue('port');
  var device = JSON.parse(device_json);
  var operand = block.getFieldValue('operand')
  var init_device = "device_sensor = Device.objects.all().get(pk="+device["id"]+")";
  var init_port = "port_src = '"+device["port_gpio"]+"'";
  var init_operand = "operand = "+operand;
  return init_device+"\n"+init_port+"\n"+init_operand+"\n";
};

Blockly.Python['when_door'] = function(block) {
  var port = block.getFieldValue('port');
  var setup_gpio = gpio_in(port);
  var wait = wait_until_pressed(port);
  return setup_gpio+'\n'+wait;
};

Blockly.Python['when_human'] = function(block) {
  var port = block.getFieldValue('port');
  var setup_gpio = gpio_in(port);
  var wait = wait_until_pressed(port);
  return setup_gpio+'\n'+wait;
}

Blockly.Python['do_lock'] = function(block) {
  var port = block.getFieldValue('port');
  var action = block.getFieldValue('action');
  var setup_gpio = gpio_out(port);
  var signal = send_signal(port, action);
  return setup_gpio+"\n"+signal;
}

Blockly.Python['do_led'] = function(block) {
  var device_actuator = JSON.parse(block.getFieldValue('port'));
  var action = block.getFieldValue('action');
  var send = []
  var device_previous = JSON.parse(block.parentBlock_.getFieldValue('port'));
  var device_previous_type = device_previous['device_type']['category'];
  var child_length = block.childBlocks_.length;
  var isLocalVar = isLocal(block);

   if (isLocalVar){
     send.push("import requests, json, os.path, atexit");
     send.push("from subprocess import Popen")
     send.push("device_actuator = Device.objects.all().get(pk="+device_actuator["id"]+")");
     send.push("current_dir_actuator = 'current/'+str(device_actuator.id)");
     send.push("current_file_actuator = current_dir_actuator+'/actuator_action_'+str(device_sensor.id)+'.py'");
     send.push("f = open(current_file_actuator, 'w')");
     send.push("f.write("+actuator_action(device_actuator['port_gpio'], action)+")");
     send.push("f.close()");
   }else{
    var port_gpio_external = device_actuator['port_gpio']+"-"+device_actuator['pan_id']
  }
  if (child_length == 0 && device_previous_type == 0){
    // only component
    send.push("url = 'http://localhost:8000/devices/"+device_previous['id']+"/'");
    send.push("data = { u'connected_devices' : u'"+device_actuator['port_zmq']+"' }");
    send.push("data = json.dumps(data)");
    send.push("headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'data':str(data) }");
    send.push("requests.put(url, data=data, headers=headers)");
    send.push("current_dir_sensor = 'current/'+str(device_sensor.id)");
    send.push("port_gpio = device_sensor.port_gpio");
    send.push("port_zmq = device_sensor.port_zmq");
    send.push("current_file_sensor = current_dir_sensor+'/sensor_action.py'");
    send.push("if not os.path.isfile(current_file_sensor):");
    send.push("\tf = open(current_file_sensor, 'w')");
    send.push("\tf.write("+sensor_action(device_previous['id'], device_previous['port_gpio'], device_previous['port_zmq'])+")");
    send.push("\tf.close()");
    send.push("\tprocess = Popen(['python', current_file_sensor, '&'])");
    send.push("\tatexit.register(process.terminate)");
    return combine(send);
  }else if (child_length == 0 && device_previous_type == 1){
    // last component but not the only one
    var port_zmqs = [];
    port_zmqs.push(device_actuator['port_zmq']);
    var temp_block = block.parentBlock_;
    while (temp_block.type == "do_led"){
      port_zmqs.push(getZMQPort(temp_block));
      temp_block = temp_block.parentBlock_;
    }
    var device_sensor = JSON.parse(temp_block.getFieldValue('port'));
    send.push("url = 'http://localhost:8000/devices/"+device_sensor['id']+"/'");
    send.push("data = { 'connected_devices':'"+parseArrayToCommas(port_zmqs)+"' }");
    send.push("data = json.dumps(data)");
    send.push("headers = { 'Content-Type': 'application/json', 'data':data }");
    send.push("requests.put(url, data=data, headers=headers)");
    send.push("current_dir_sensor = 'current/'+str(device_sensor.id)");
    send.push("port_gpio = device_sensor.port_gpio");
    send.push("port_zmq = device_sensor.port_zmq");
    send.push("current_file_sensor = current_dir_sensor+'/sensor_action.py'");
    send.push("if not os.path.isfile(current_file_sensor):");
    send.push("\tf = open(current_file_sensor, 'w')");
    send.push("\tf.write("+sensor_action(device_sensor['id'], device_sensor['port_gpio'], device_sensor['port_zmq'])+")");
    send.push("\tf.close()");
    send.push("\tprocess = Popen(['python', current_file_sensor, '&'])");
    send.push("\tatexit.register(process.terminate)");
    return combine(send);
  }
  return combine(send);
}
