'use strict';

goog.require('Blockly.Python');

function add_parentheses(string){
   return '\"'+string+'\"';
}

function add_single_parentheses(string){
   return '\\"'+string+'\\"';
}

function combine(strings){
   ret = '';
   for (i=0; i<strings.length; i++){
     ret = ret + strings[i] + "\n"
   }
   return ret;
}

function add_newlines(strings, flag){
   ret = '';
   for (i = 0; i < strings.length; i++){
	if (flag == 0)
	  ret = ret + add_parentheses(strings[i])+"+"+add_parentheses("\\n");
	else
	  ret = ret + strings[i]+"+"+add_parentheses("\\n");
	if (i != strings.length-1)
	  ret = ret + "+";
   }
   return ret;
}

function add_newlines_single(strings, flag){
   ret = '';
   for (i = 0; i < strings.length; i++){
        if (flag == 0)
          ret = ret + add_single_parentheses(strings[i])+"+"+add_single_parentheses("\\n");
        else
          ret = ret + strings[i]+"+"+add_single_parentheses("\\n");
        if (i != strings.length-1)
          ret = ret + "+";
   }
   return ret;
}

function send_to_zmq(port){
  var send = [];
  send.push('def send_to_zmq(body):');
  send.push('\\tprint('+port+')');
  send.push('\\tcontext = zmq.Context()');
  send.push('\\tsocket = context.socket(zmq.REQ)');
  send.push("\\tsocket.connect('tcp://127.0.0.1:"+port+"')");
  send.push('\\tsocket.send(body)');
  send.push('\\tmessage = socket.recv()');
  send.push("\\tprint('Received reply %s [ %s ]' % (body, message))");

  return add_newlines(send, 0);
}

function parseArrayToCommas(array){
  var ret = "";
  for(i=0; i<array.length; i++){
    ret = ret + array[i];
    if (i != array.length-1)
      ret = ret + ",";
  }
  return ret;
}

function callback(uid_sensor, id_sensor){
  var send = [];
  send.push('def callback():');
  send.push("\\tprint('callback')");
  send.push('\\tbody = {}');
  send.push('\\tnew_body = {}');
  send.push("\\tnew_body['message_type'] = 'event'")
  send.push("\\tdevice_json = urllib2.urlopen('http://localhost:8000/devices/"+id_sensor+"/')");
  send.push('\\tdevice = json.load(device_json)');
  send.push("\\tbody['sender'] = '"+uid_sensor+"'");
  send.push("\\tbody['target'] = device['connected_devices']");
  send.push("\\tnew_body['body'] = body")
  send.push('\\tbody_json = json.dumps(new_body)');
  send.push('\\tsend_to_zmq(body_json)');
  send.push("\\tprint('sent')")

  return add_newlines(send, 0);
}

function sensor_action(uid_sensor, id_sensor, port_gpio, port_zmq){
  var send = [];
  send.push(add_parentheses('import RPi.GPIO as GPIO'));
  send.push(add_parentheses('import zmq, time, sys, json, urllib2'));
  send.push(send_to_zmq(port_zmq));
  send.push(callback(uid_sensor, id_sensor));
  send.push(add_parentheses('GPIO.setmode(GPIO.BCM)'));
  send.push(add_parentheses('GPIO.setwarnings(False)'));
  send.push(add_parentheses('GPIO.setup('+port_gpio+', GPIO.IN, pull_up_down=GPIO.PUD_UP)'));
  send.push(add_parentheses('while True:'));
  send.push(add_parentheses('\\tinput_state = GPIO.input('+port_gpio+')'));
  send.push(add_parentheses('\\tif input_state == GPIO.LOW:'));
  send.push(add_parentheses("\\t\\tprint('clicked')"));
  send.push(add_parentheses('\\t\\tcallback()'));
  send.push(add_parentheses('\\t\\twhile input_state == GPIO.LOW:'));
  send.push(add_parentheses('\\t\\t\\tinput_state = GPIO.input('+port_gpio+')'));
  send.push(add_parentheses("\\ttime.sleep(0.2)"));
  return add_newlines(send, 1);
}

function actuator_action(port_gpio, code){
  console.log("ACTUATOR ACTION")
  var send = [];
  send.push(add_parentheses("import RPi.GPIO as GPIO"));
  send.push(add_parentheses("import time"));
  send.push(add_parentheses("GPIO.setmode(GPIO.BCM)"));
  send.push(add_parentheses("GPIO.setwarnings(False)"));
  send.push(add_parentheses("channel = (int) ("+port_gpio+")"));
  send.push(add_parentheses("GPIO.setup(channel, GPIO.OUT)"));
  send.push(add_parentheses("print('LED NYALA')"));
  if (code == 0){ // turn on
    send.push(add_parentheses("GPIO.output("+port_gpio+", GPIO.HIGH)"));
  }else if (code == 1){ // turn off
    send.push(add_parentheses("GPIO.output("+port_gpio+", GPIO.LOW)"));
  }
  else if (code == 2){ //blink
    send.push(add_parentheses("GPIO.output("+port_gpio+", GPIO.HIGH)"));
    send.push(add_parentheses("time.sleep(1)"));
    send.push(add_parentheses("GPIO.output("+port_gpio+", GPIO.LOW)"));
  }
  else if (code == 3){ //disco
    send.push(add_parentheses("count = 0"));
    send.push(add_parentheses("while count<10:"));
    send.push(add_parentheses("\\tGPIO.output("+port_gpio+", GPIO.HIGH)"));
    send.push(add_parentheses("\\ttime.sleep(0.5)"));
    send.push(add_parentheses("\\tGPIO.output("+port_gpio+", GPIO.LOW)"));
    send.push(add_parentheses("\\ttime.sleep(0.5)"));
    send.push(add_parentheses("\\tcount\+=1"));
  }
  return add_newlines(send, 1);
}

function actuator_action_out(port_gpio, code){
  var send = [];
  send.push(add_single_parentheses("import RPi.GPIO as GPIO"));
  send.push(add_single_parentheses("import time"));
  send.push(add_single_parentheses("GPIO.setmode(GPIO.BCM)"));
  send.push(add_single_parentheses("GPIO.setwarnings(False)"));
  send.push(add_single_parentheses("channel = (int) ("+port_gpio+")"));
  send.push(add_single_parentheses("GPIO.setup(channel, GPIO.OUT)"));
  if (code == 0){ // turn on
    send.push(add_single_parentheses("GPIO.output("+port_gpio+", GPIO.HIGH)"));
  }else if (code == 1){ // turn off
    send.push(add_single_parentheses("GPIO.output("+port_gpio+", GPIO.LOW)"));
  }
  else if (code == 2){ //blink
    send.push(add_single_parentheses("GPIO.output("+port_gpio+", GPIO.HIGH)"));
    send.push(add_single_parentheses("time.sleep(1)"));
    send.push(add_single_parentheses("GPIO.output("+port_gpio+", GPIO.LOW)"));
  }
  else if (code == 3){ //disco
    send.push(add_single_parentheses("count=0"));
    send.push(add_single_parentheses("while count<10:"));
    send.push(add_single_parentheses("\\tGPIO.output("+port_gpio+", GPIO.HIGH)"));
    send.push(add_single_parentheses("\\ttime.sleep(0.5)"));
    send.push(add_single_parentheses("\\tGPIO.output("+port_gpio+", GPIO.LOW)"));
    send.push(add_single_parentheses("\\ttime.sleep(0.5)"));
    send.push(add_single_parentheses("\\tcount\+=1"));
  }
  return add_newlines_single(send, 1);
}

function getDeviceType(block){
  return JSON.parse(block.getFieldValue('port'))['device_type']['name'];
}

function getZMQPort(block){
  device = JSON.parse(block.getFieldValue('port'));
  if(device['isLocalVar']){
    return device['port_zmq'];
  }
  return device['port_zmq']+"-"+device['uid'];
}

function isLocal(block){
  return JSON.parse(block.getFieldValue('port'))['is_local'];
}

function gpio_in(channel){
  var channel_declaration = "gpio_pin="+channel;
  var set_mode = "GPIO.setmode(GPIO.BCM)";
  var setup = "GPIO.setup(gpio_pin, GPIO.IN)";
  return channel_declaration+"\n"+set_mode+"\n"+setup;
}

function gpio_out(channel){
  var channel_declaration = "gpio_pin="+channel;
  var set_mode = "GPIO.setmode(GPIO.BCM)";
  var setup = "GPIO.setup(gpio_pin, GPIO.OUT)"
  return channel_declaration+"\n"+set_mode+"\n"+setup;
}

function wait_until_pressed(channel){
  return 'while GPIO.input('+channel+') == False:\n time.sleep(0.2)';
}

function send_signal(channel, signal){
  if (signal == 1)
    return "GPIO.output("+channel+", GPIO.HIGH)";
  return "GPIO.output("+channel+", GPIO.LOW)";
}

Blockly.Python['when_btn'] = function(block) {
  var device_json = block.getFieldValue('port');
  var device = JSON.parse(device_json);
  var operand = block.getFieldValue('operand')
  var init_device = "device_sensor = Device.objects.all().get(pk="+device["id"]+")";
  var init_port = "port_src = '"+device["port_gpio"]+"'";
  var init_operand = "operand = "+operand;
  return init_device+"\n"+init_port+"\n"+init_operand+"\n";
};

Blockly.Python['when_reader'] = function(block) {
  var device_json = block.getFieldValue('port');
  var device = JSON.parse(device_json);
  var init_device = "device_sensor = Device.objects.all().get(pk="+device['id']+")";
  return init_device+"\n";
};

Blockly.Python['do_led'] = function(block) {
  var device_actuator = JSON.parse(block.getFieldValue('port'));
  var action = block.getFieldValue('action');
  var send = []
  var device_previous = JSON.parse(block.parentBlock_.getFieldValue('port'));
  var device_previous_type = device_previous['device_type']['category'];
  var child_length = block.childBlocks_.length;
  var isLocalVar = isLocal(block);

  send.push("import requests, json, os.path, atexit");
  send.push("from subprocess import Popen");
  if (isLocalVar){
     console.log("LOCAL VAR");
     send.push("device_actuator = Device.objects.all().get(pk="+device_actuator["id"]+")");
     send.push("current_dir_actuator = 'current/'+str(device_actuator.uid)");
     send.push("current_file_actuator = current_dir_actuator+'/actuator_action_'+str(device_sensor.uid)+'.py'");
     send.push("f = open(current_file_actuator, 'w')");
     send.push("f.write("+actuator_action(device_actuator['port_gpio'], action)+")");
     send.push("f.close()");
     console.log("DONE WRITE");
   }else{
     var message = new Object();
     var code = actuator_action_out(device_actuator['port_gpio'], action);
     message.from = device_previous;
     message.to = device_actuator;
     message.code = code;
     message = JSON.stringify(message);
     send.push("sendAction('"+message+"')");
  }
  
  if (child_length == 0 && (device_previous_type == 0 || device_previous_type == 2) ){
    // only component
    send.push("url = 'http://localhost:8000/devices/"+device_previous['id']+"/'");
    console.log(device_previous['id']);
    if (isLocalVar){
      send.push("data = { u'connected_devices' : u'"+device_actuator['port_zmq']+"' }");
    }else{
      send.push("data = { u'connected_devices' : u'"+device_actuator['port_zmq']+"-"+device_actuator['uid']+"' }");
    }
    console.log(device_actuator['port_zmq']);
    send.push("data = json.dumps(data)");
    send.push("headers = { 'Content-Type': 'application/x-www-form-urlencoded', 'data':str(data) }");
    send.push("requests.put(url, data=data, headers=headers)");
    send.push("current_dir_sensor = 'current/'+str(device_sensor.uid)");
    send.push("port_zmq = device_sensor.port_zmq");
    if (device_previous_type == 0){//pushButton
      send.push("port_gpio = device_sensor.port_gpio");
      send.push("current_file_sensor = current_dir_sensor+'/sensor_action.py'");
      send.push("if not os.path.isfile(current_file_sensor):");
      send.push("\tprint('file didnt exist')")
      send.push("\tf = open(current_file_sensor, 'w')");
      send.push("\tf.write("+sensor_action(device_previous['uid'], device_previous['id'], device_previous['port_gpio'], device_previous['port_zmq'])+")");
      send.push("\tf.close()");
      send.push("\tprocess = Popen(['python', current_file_sensor, '&'])");
      send.push("\tatexit.register(process.terminate)");
    }
    return combine(send);
  }else if (child_length == 0 && device_previous_type == 1){
    // last component but not the only one
    var port_zmqs = [];
    port_zmqs.push(device_actuator['port_zmq']);
    var temp_block = block.parentBlock_;
    while (temp_block.type == "do_led"){
      port_zmqs.push(getZMQPort(temp_block));
      temp_block = temp_block.parentBlock_;
    }
    var device_sensor = JSON.parse(temp_block.getFieldValue('port'));
    send.push("url = 'http://localhost:8000/devices/"+device_sensor['id']+"/'");
    send.push("data = { 'connected_devices':'"+parseArrayToCommas(port_zmqs)+"' }");
    send.push("data = json.dumps(data)");
    send.push("headers = { 'Content-Type': 'application/json', 'data':data }");
    send.push("requests.put(url, data=data, headers=headers)");
    send.push("current_dir_sensor = 'current/'+str(device_sensor.uid)");
    send.push("port_zmq = device_sensor.port_zmq");
    if (device_previous_type == 0){//pushButton
      send.push("port_gpio = device_sensor.port_gpio");
      send.push("current_file_sensor = current_dir_sensor+'/sensor_action.py'");
      send.push("if not os.path.isfile(current_file_sensor):");
      send.push("\tprint('file didnt exist')");
      send.push("\tf = open(current_file_sensor, 'w')");
      send.push("\tf.write("+sensor_action(device_sensor['uid'], device_sensor['id'], device_sensor['port_gpio'], device_sensor['port_zmq'])+")");
      send.push("\tf.close()");
      send.push("\tprocess = Popen(['python', current_file_sensor, '&'])");
      send.push("\tatexit.register(process.terminate)");
    }
    return combine(send);
  }
  return combine(send);
}
