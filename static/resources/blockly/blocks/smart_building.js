var getUrl = window.location;
var BASE_URL = getUrl .protocol + "//" + getUrl.host + "/";

var DEVICE_BTN_URL = BASE_URL+"devices_btn/";
var DEVICE_BTN_OUTSIDE_URL = BASE_URL+"getoutside_devices/?q=button";
var DEVICE_LED_URL = BASE_URL+"devices_led/";
var DEVICE_LED_OUTSIDE_URL = BASE_URL+"getoutside_devices/?q=led";
var READER_URL = BASE_URL+"reader/"

var buttons = [];

function parseDevice(devices){
  ret = [];
  for(i=0; i<devices.length; i++){
    if (devices[i].is_local){
      ret.push([devices[i].name, JSON.stringify(devices[i])]);
    }else{
      ret.push([devices[i].name+"-"+devices[i].pan_id, JSON.stringify(devices[i])]);
    }
  }
  return ret;
}

$.get(DEVICE_BTN_URL , function(data, status){
        for(i=0; i < data.length; i++){
	  data[i].is_local = true;
          buttons.push(data[i]);
        }
    });

Blockly.Blocks['when_btn'] = {
  init: function() {
    if (buttons.length > 0){
        this.appendDummyInput()
            .appendField('When')
	    .appendField('Button No.')
            .appendField(new Blockly.FieldDropdown(parseDevice(buttons)),'port')
            .appendField("Is")
	    .appendField(new Blockly.FieldDropdown([["Pressed", "True"], ["Not Pressed", "False"]]), "operand");
    }
    this.setNextStatement(true);
    this.setColour(160);
    this.setTooltip('Returns number of letters in the provided text.');
    this.setHelpUrl('http://www.w3schools.com/jsref/jsref_length_string.asp');
  }
};

var readers = [];

$.get(READER_URL, function(data, status){
    for(i=0; i<data.length; i++){
        data[i].is_local = true;
	readers.push(data[i])
    }
});

Blockly.Blocks['when_reader'] = {
  init: function() {
    if (readers.length > 0){
        this.appendDummyInput()
	    .appendField("When")
	    .appendField("Reader No.")
	    .appendField(new Blockly.FieldDropdown(parseDevice(readers)), 'port')
	    .appendField("Has Read")
    }
    this.setNextStatement(true);
    this.setColour(160);
    this.setTooltip('Card read on reader');
    this.setHelpUrl('http://www.w3schools.com/jsref/jsref_length_string.asp');
  }
};

var leds = [];

$.get(DEVICE_LED_URL, function(data, status){
        for(i=0; i < data.length; i++){
	  data[i].is_local = true;
          leds.push(data[i]);
        }
    });

$.get(DEVICE_LED_OUTSIDE_URL, function(data, status){
        for(i=0; i < data.length; i++){
          data[i].is_local = false;
          leds.push(data[i])
        }
});

Blockly.Blocks['do_led'] = {
  init: function() {
  	this.setPreviousStatement(true);
  	this.setNextStatement(true);
	if (leds.length > 0){
	    this.appendDummyInput()
	     .appendField(new Blockly.FieldDropdown([
                        ['Turn On', '0'],
                        ['Turn Off', '1'],
		                ['Blink For 1 second', '2'],
		                //["Let the disco begin on ", '3'],
                     ]),
                     'action')
	    .appendField('LED Number')
	    .appendField(new Blockly.FieldDropdown(parseDevice(leds)), 'port');
	}
    this.setColour(160);
    this.setTooltip('Returns number of letters in the provided text.');
    this.setHelpUrl('http://www.w3schools.com/jsref/jsref_length_string.asp');
  }
};
