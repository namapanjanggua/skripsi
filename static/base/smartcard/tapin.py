#edit by guhkun
from sys import stdin, exc_info
from time import sleep
import time, sys

from smartcard.CardMonitoring import CardMonitor, CardObserver
# from smartcard.util import *
# from smartcard.scard import SCARD_CTL_CODE
from smartcard.System import readers

from reader import *
from utils import *

##### APDU #####

# KARTU UI (LAMA)
#KTM_ATR = [0x3B, 0x8A, 0x80, 0x01, 0x4A, 0x43, 0x4F, 0x50, 0x33, 0x31, 0x56, 0x32, 0x33, 0x32, 0x7A]

SELECT_CARD_UID = [0xFF, 0xCA,  0x00, 0x00, 0x00]
SELECT_CARD_APPLET_MAHASISWA = [0x00, 0xA4, 0x04, 0x00, 0x06, 0xF3, 0x60, 0x00, 0x00, 0x01, 0x01]

# TIPE: paramater yang menentukan untuk Masuk atau Keluar parkir
TIPE = 'masuk'

class printobserver(CardObserver):
    print "start observe"

    isFirst = True
    prop = ""
    kode_identitas = ""

    def __init__(self, port_zmq):
	self.port_zmq = port_zmq

    # INIT CARD
    def initialize(self, card):
        print "initialize"
        connection = card.createConnection()
        connection.connect()

        send_control(connection, [CLEAR_LCD, MESSAGE_SILAHKAN, MESSAGE_KARTU])
        connection.disconnect()

    def process(self, card):
        print "process"
        connection = card.createConnection()
        connection.connect()

        send_control(connection, [CLEAR_LCD, MESSAGE_PROCESSING_1, MESSAGE_PROCESSING_2])

        card_uid, sw1, sw2 = connection.transmit(SELECT_CARD_UID)

        if sw1 != 0x90:
            self.send_control(connection, [CLEAR_LCD, MESSAGE_CARD_ERROR_1, MESSAGE_CARD_ERROR_2])
            connection.disconnect()
            return

        resp_spek_ui, sw1_spek_ui, sw2_spek_ui = connection.transmit(SELECT_CARD_APPLET_MAHASISWA)
        resp_konsorsium, sw1_konsorsium, sw2_konsorsium = connection.transmit(SELECT_CARD_KONSORSIUM)

        # print ('SELECT_CARD_APPLET : ' , resp_spek_ui)
        print "select reader"
        if sw1_spek_ui == 0x90:
            print "UI"
            return proses_card_ui(connection, card_uid)

        elif sw1_konsorsium == 0x90 or (sw1_konsorsium == 0x61 and sw2_konsorsium == 0x22):
            # 0x90 => Kartu PT DAM
            # 0x61 0x22 => Kartu PT Xirka
            print "konsorsium"
            return proses_card_konsorsium(connection, card_uid)

        send_control(connection, [CLEAR_LCD, MESSAGE_CARD_ERROR_1, MESSAGE_CARD_ERROR_2])
	connection.disconnect()
        return

    def update(self, observable, (addedcards, removedcards)):
        for card in addedcards:
	    if not card.atr:
		#empty or not a card yet
		print "empty card"
		continue
	    print "detect card"
            if self.isFirst:
                self.initialize(card)
                self.prop = card
                self.isFirst = False
            else:
                self.cuid, self.kode_identitas, self.nama, self.kode_org  = self.process(card)
                connection = card.createConnection()
                connection.connect()
                show_msg_tap(connection, TIPE, self.prop, self.cuid, self.kode_identitas, self.nama, self.kode_org, self.port_zmq)

try:
    cardmonitor = CardMonitor()
    cardobserver = printobserver(sys.argv[1])
    cardmonitor.addObserver(cardobserver)
    while True:
        sleep(10)
except:
    print exc_info()[0], ':', exc_info()[1]
