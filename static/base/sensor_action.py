from smartbuilding.models import Device
import RPi.GPIO as GPIO
import zmq, time, sys, json

def send_to_zmq(port, body):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:"+port)
    socket.send(body)
    message = socket.recv()
    print("Received reply %s [ %s ]" % (body, message))

def callback(port, port_target):
    body = {}
    body['target'] = port_target
    devices = Device.objects.get(pk=17)
    print(devices.name)
    body_json = json.dumps(body)
    send_to_zmq(port,body_json)

if input_state == False:
    callback(port, port_target)
    time.sleep(0.2)
    input_state = GPIO.input(channel)
    while input_state == False:
        input_state = GPIO.input(channel)
