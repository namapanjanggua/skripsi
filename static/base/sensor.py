import RPi.GPIO as GPIO
import time, sys, os
from subprocess import call

def setup_gpio(channel):
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    GPIO.setup(channel, GPIO.IN, pull_up_down=GPIO.PUD_UP)

channel = (int)(sys.argv[1])
port = sys.argv[2]
#setup_gpio(channel)
dir_path = os.path.dirname(os.path.realpath(__file__))
current_file = dir_path+"/sensor_action.py"
while not os.path.exists(current_file):
    time.sleep(0.1)
call(["python", current_file])
