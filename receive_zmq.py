import zmq, time, pika, json, redis

def send_to_rabbit(channel, body):
    channel.queue_declare(queue="broadcast")
    channel.basic_publish(exchange='',
                          routing_key='broadcast',
                          body=body)

def create_channel_rabbit():
    params = pika.ConnectionParameters("localhost")
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    return channel

def send_zmq(port, body):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:"+str(port))
    socket.send(body)
    socket.recv()
    print("sent to "+str(port))

def receive_zmq():
    channel = create_channel_rabbit()
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://127.0.0.1:5555")

    print "ZeroMQ receiving messages on port 5555"

    while True:
        #  Wait for next request from client
	print("Here")
        message = socket.recv()
	print("There")
        print("Received request: %s" % message)
        #  Do some 'work'
        time.sleep(0.5)
        body = json.loads(message)
        type = body['message_type']

        if type == "broadcast_device":
            try:
                send_to_rabbit(channel, message)
                print("sent...")
            except:
                print "creating new channel..."
                channel = create_channel_rabbit()
                send_to_rabbit(channel, message)
                print("sent...")

        elif type == "receive_event":
            target = body['body']['target']
            r = redis.StrictRedis()
            target = json.loads(r.get("device_"+target))
            send_zmq(target['device']['port_zmq'], message)
        socket.send("Echo: " + message)

def start_publishing():
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://127.0.0.1:5556")

    time.sleep(0.5)
    return socket

publisher = start_publishing()
receive_zmq()
