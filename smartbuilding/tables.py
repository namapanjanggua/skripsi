from table import Table
from table.columns import Column, LinkColumn, Link
from table.utils import A
from models import GPIODevice, Device

class DeviceTable(Table):
    id = Column(field='id', header='Device ID')
    name = Column(field='name', header="Device Name")
    port_gpio = Column(field='port_gpio', header="Device GPIO Port")
    port_zmq = Column(field='port_zmq', header="Device ZeroMQ Port")
    pid_zmq = Column(field='pid_zmq', header="Device ZeroMQ PID")
    device_type = Column(field="device_type.name", header="Device Type")
    action = LinkColumn(header=u'Action', links=[Link(text=u'Init', viewname='init' ,args=(A('id'),)),])

    class Meta:
	model = Device

class GPIOTable(Table):
    port = Column(field='port', header="Port")
    deviceID = Column(field='deviceID', header="Device ID")

    class Meta:
	model = GPIODevice