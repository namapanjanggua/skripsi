from views import ManufacturerViewSet, DeviceTypeViewSet, DeviceViewSet
from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from . import views
from django.conf.urls import include

router = DefaultRouter()
router.register(r'manufacturers', ManufacturerViewSet, base_name='manufacturers')
router.register(r'device_types', DeviceTypeViewSet, base_name='device_types')
router.register(r'devices', DeviceViewSet, base_name='devices')
urlpatterns = [
    url(r'^manufacturers/new/$', views.manufacturer_new, name='manufacturer_new'),
    url(r'^device_types/new/$', views.devices_type_new, name="devices_type_new"),
    url(r'^devices/new/$', views.device_new, name="device_new"),
    url(r'^readers/new/$', views.reader_new, name="reader_new"),
    url(r'^devices/list/', views.device_list, name="device_list"),
    url(r'^readers/access_list/', views.reader_access_list, name="reader_access_list"),
    url(r'^gpio/list/', views.gpio_list, name="gpio_list"),
    url(r'', include(router.urls)),
    url(r'^reader/', views.devices_reader, name="devices_reader"),
    url(r'^add_card/', views.add_card, name="add_card"),
    url(r'^delete_card/(\d+)/(\d+)$', views.delete_card, name="delete_card"),
    url(r'^devices_btn/', views.devices_btn, name='devices_btn'),
    url(r'^devices_door/', views.devices_door, name="devices_door"),
    url(r'^devices_human/', views.devices_human, name="devices_human"),
    url(r'^devices_led/', views.devices_led, name="devices_led"),
    url(r'^init/(\d+)/$', views.init, name="init"),
    url(r'^init_all/', views.initAll, name="init_all"),
    url(r'^getoutside_devices/', views.getoutside_devices, name="get_outside"),
    url(r'^exec/', views.execute, name="exec"),
    url(r'^send_action/', views.send_action, name="send_action"),
    url(r'^main/', views.main, name="main"),
]
