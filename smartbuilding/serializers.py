from rest_framework import serializers
from datetime import datetime
from models import Manufacturer, Device_type, Device

class ManufacturerSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    is_active = serializers.BooleanField(default=True)
    created_date = serializers.DateTimeField(default=datetime.now())
    updated_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Manufacturer
        fields = '__all__'

class DeviceTypeSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    manufacturer = ManufacturerSerializer(many=False, read_only=True)
    value_types = serializers.CharField()
    is_active = serializers.BooleanField(default=True)
    created_date = serializers.DateTimeField(default=datetime.now())
    updated_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Device_type
        fields = '__all__'

    def create(self, validated_data):
        name = validated_data.get('name')
        value_types = validated_data.get('value_types')
        manufacturer = Manufacturer.objects.all().filter(id=validated_data.get('manufacturer')).get()
        obj = Device_type.objects.create(name=name,value_types=value_types,  manufacturer=manufacturer)
        return obj


class DeviceSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    port_gpio = serializers.IntegerField()
    port_zmq = serializers.IntegerField(default=None)
    pid_zmq = serializers.IntegerField(default=None)
    pan_id = serializers.CharField()
    uid = serializers.CharField(default=None)
    device_type = DeviceTypeSerializer(many=False, read_only=True)
    is_active = serializers.BooleanField(default=True)
    created_date = serializers.DateTimeField(default=datetime.now())
    updated_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Device
        fields = '__all__'

    def create(self, validated_data):
        name = validated_data.get('name')
        device_type = Device_type.objects.all().filter(id=validated_data.get('device_type')).get()
        obj = Device.objects.create(name=name, device_type=device_type)
        return obj