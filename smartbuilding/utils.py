from random import randint
import socket, netifaces, hashlib

def isSocketAvailable(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.bind(("127.0.0.1", port))
    except:
        return False
    sock.close()
    return True

def getRandomPort():
    port = randint(5000, 6000)
    while(not isSocketAvailable(port)):
        port = randint(1, 6000)
    return port

def getMacAddress():
    return netifaces.ifaddresses('eth0')[netifaces.AF_LINK][0]['addr']

def getPANAddress():
    mac = getMacAddress()
    encryptor = hashlib.sha1()
    encryptor.update(mac)
    return encryptor.hexdigest()

def getUID(deviceID):
    mac = getMacAddress()
    dict = mac+"_"+str(deviceID)
    encryptor = hashlib.sha1()
    encryptor.update(dict)
    return encryptor.hexdigest()
