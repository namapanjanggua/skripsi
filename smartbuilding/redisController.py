from models import GPIODevice, Card
import redis, json

def init():
    return redis.StrictRedis(host='localhost', port=6379)

def isPortAttached(portNumber):
    r = init()
    if r.get("port-"+str(portNumber)) is None:
        return False
    return True

def attachPort(portNumber, deviceID):
    r = init()
    r.set("port-"+portNumber, deviceID)

def getAllDevice():
    r = init()
    devices = []
    for key in r.scan_iter("port-*"):
        device = GPIODevice()
        portNumber = key.split("-")[1]
        device.port = portNumber
        device.deviceID = r.get(key)
        devices.append(device)
    return devices

def deleteCard(reader_id, kode_identitas):
    r = init()
    r.delete("CARD_"+reader_id+"_"+kode_identitas)

def addNewCard(nama, kode_identitas, id_reader):
    r = init()
    object = {}
    object['nama'] = nama
    object['kode_identitas'] = kode_identitas
    object['reader_id'] = id_reader
    object = json.dumps(object)
    r.set("CARD_"+str(id_reader)+"_"+str(kode_identitas), object)

def getAllCard():
    r = redis.StrictRedis()
    cards = []
    for key in r.scan_iter("CARD_*_*"):
        object = r.get(key)
        object = json.loads(object)
	card = Card()
        card.nama = object['nama']
	card.kode_identitas = object['kode_identitas']
        card.reader_id = object['reader_id']
	cards.append(card)
    return cards

def populateGPIO():
    CHOICES_GPIO = []

    i = 2
    while i <= 25:
        if not isPortAttached(i):
            CHOICES_GPIO.append([i, "GPIO "+str(i)])
        i = i+1
    return CHOICES_GPIO

def getOutsideDevice(query):
    r = init()
    devices = []
    query = query.lower()
    for key in r.scan_iter("device_*"):
        device = r.get(key)
        device = json.loads(device)
        device = device['device']
        type = device['device_type']['name'].lower()
        if type == query:
            devices.append(device)
    return devices
