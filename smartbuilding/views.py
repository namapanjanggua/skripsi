from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework import status
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from datetime import datetime
from serializers import ManufacturerSerializer, DeviceTypeSerializer, DeviceSerializer
from models import Manufacturer, Device_type, Device
from forms import ManufacturerForm, DeviceTypeForm, DeviceForm, ReaderForm, CardForm
from django.shortcuts import render, redirect
from redisController import attachPort, getAllDevice, populateGPIO, getOutsideDevice, getAllCard, addNewCard, deleteCard
from tables import GPIOTable, DeviceTable
from subprocess import call, Popen
from utils import getRandomPort, getPANAddress, getUID
from rabbitmqController import reportRoom, sendAction
from django.views.decorators.csrf import csrf_exempt
import json, atexit, threading
from smartcard.System import readers

def clear():
    print("Clearing ZMQ data")
    devices = Device.objects.all()
    for device in devices:
        device.port_zmq = None
        device.pid_zmq = None
        device.connected_devices = None
        device.save()
    print("Done")

clear()
reportRoom()
threading.Timer(300, reportRoom).start()

def main(request):
    return render(request, 'blockly/main/base.html')

def manufacturer_new(request):
    if request.method == "POST":
        form = ManufacturerForm(request.POST)
        if form.is_valid():
            manufacturer = form.save(commit=False)
            manufacturer.name = request.POST.get('name', '')
            manufacturer.save()

            return HttpResponse('Success Add Data!')
        return HttpResponse('Failed!')
    else:
        form = ManufacturerForm()
        return render(request, 'form/new_form.html', {'form': form, 'model': 'Manufacturer'})

@csrf_exempt
def execute(request):
    if request.method == "POST":
        command = request.POST.get("command", '')
        exec(command)
        return HttpResponse("Execution Success!")
    return HttpResponse("Method not allowed!")

@csrf_exempt
def send_action(request):
    if request.method == "POST":
        message = request.POST.get("message", '')
        sendAction(message)
        return HttpResponse("Message sent!")
    return HttpResponse("Method not allowed!")

def devices_type_new(request):
    if request.method == "POST":
        form = DeviceTypeForm(request.POST)
        if form.is_valid():
            manufacturer_id = request.POST.get('manufacturer', '')
            manufacturer_instance = Manufacturer.objects.filter(id=manufacturer_id, is_active=True).first()

            device_type = Device_type()
            device_type.name = request.POST.get('name', '')
            device_type.manufacturer = manufacturer_instance
            device_type.value_types = request.POST.get('value_types', '')
            device_type.category = request.POST.get('category', '')
            device_type.save()
            return HttpResponse("Success Add Data!")
        return HttpResponse('Add Data Failed!')
    else:
        form = DeviceTypeForm()
        return render(request, 'form/new_form.html', {'form': form, 'model': 'Device Type'})

def reader_new(request):
    if request.method == "POST":
	device_type_id = request.POST.get('device_type', '')
        device_type_instance = Device_type.objects.filter(id=device_type_id, is_active=True).first()

        device = Device()
        device.name = request.POST.get('name', '')
        device.card_reader = request.POST.get('card_reader', '')
        device.device_type = device_type_instance
        device.pan_id = getPANAddress()
        device.save()

        device.uid = getUID(device.id)
        device.save()

        return HttpResponse('Success Add Data!')
    else:
	r = readers()
        form = ReaderForm(r)
        return render(request, 'form/new_form.html', {'form':form, 'model': 'Device'})


def device_new(request):
    if request.method == "POST":
        device_type_id = request.POST.get('device_type', '')
        device_type_instance = Device_type.objects.filter(id=device_type_id, is_active=True).first()

        device = Device()
        device.name = request.POST.get('name', '')
        device.port_gpio = request.POST.get('port_gpio', '')
        device.device_type = device_type_instance
        device.pan_id = getPANAddress()
        device.save()

        device.uid = getUID(device.id)
        device.save()

        attachPort(device.port_gpio, device.id)

        return HttpResponse('Success Add Data!')
    else:
        choices_gpio = populateGPIO()
        form = DeviceForm(choices_gpio)
        return render(request, 'form/new_form.html', {'form':form, 'model': 'Device'})

def delete_card(request, reader_id, kode_identitas):
    deleteCard(reader_id, kode_identitas)
    return HttpResponse("Deletion Success")

def add_card(request):
    if request.method == "POST":
        form = CardForm(request.POST)
        if form.is_valid():
            card = form.save(commit=False)
            card.nama = request.POST.get('nama', '')
            card.kode_identitas = request.POST.get('kode_identitas', '')
	    readers = Device.objects.all().filter(is_active=True, device_type__name__icontains="card reader").first()
	    addNewCard(card.nama, card.kode_identitas, readers.id)
            return HttpResponse('Success Add Data!')
        return HttpResponse('Failed!')
    else:
        form = CardForm()
        return render(request, 'form/new_form.html', {'form': form, 'model': 'Card'})

def reader_access_list(request):
    cards = getAllCard()
    return render(request, 'table/reader_access_list.html', {'cards': cards, 'model':'Card'})

def device_list(request):
    devices = Device.objects.all().filter(is_active=True)
    return render(request, 'table/device_list.html', {'devices': devices, 'model':'Device'})

def gpio_list(request):
    gpio = getAllDevice()

    gpioTable = GPIOTable(gpio)
    print gpioTable
    return render(request, 'table/base.html', {'object_table': gpioTable, 'model':'GPIO'})

def _init(id):
    device = Device.objects.all().get(pk=id)

    if not device.pid_zmq is None:
        call(["kill", str(device.pid_zmq)])

    current_dir = "current/"+device.uid
    call(["rm", "-rf", current_dir])
    call(["mkdir", current_dir])

    port_zmq = getRandomPort()

    if device.device_type.category == "1": #actuator
        call(["cp", "static/base/create_actuator.py", current_dir])
        current_file = current_dir+"/create_actuator.py"
        process = Popen(["python", current_file, str(port_zmq), str(device.port_gpio), str(device.id) , "&"])
        atexit.register(process.terminate)
    elif device.device_type.category == "0": #sensor
        call(["cp", "static/base/create.py", current_dir])
        current_file = current_dir+"/create.py"
        process = Popen(["python", current_file, str(port_zmq), str(device.id), "&"])
        atexit.register(process.terminate)
    elif device.device_type.category == "2": #resource/card reader
	call(["cp", "static/base/smartcard/reader.py", current_dir])
	call(["cp", "static/base/smartcard/utils.py", current_dir])
	call(["cp", "static/base/smartcard/tapin.py", current_dir])
	call(["cp", "static/base/smartcard/create.py", current_dir])
	current_file = current_dir+"/tapin.py"
	current_file_zmq = current_dir+"/create.py"
	process = Popen(["python", current_file_zmq, str(port_zmq), str(device.id), "&"])
	process2 = Popen(["python", current_file, str(port_zmq), "&"])
	atexit.register(process.terminate)
	atexit.register(process2.terminate)

    device.port_zmq = port_zmq
    device.pid_zmq = process.pid
    device.save()

    ret = {}
    ret['port_zmq'] = port_zmq
    ret['pid_zmq'] = process.pid
    return ret

def init(request, id):
    ret = _init(str(id))
    return HttpResponse(json.dumps(ret))

def initAll(request):
    devices = Device.objects.all()
    for device in devices:
        _init(str(device.id))
    return HttpResponse("Success")

def getoutside_devices(request):
    query = request.GET.get('q','')
    print query
    outside_device = getOutsideDevice(query)

    return HttpResponse(json.dumps(outside_device),content_type="application/json")

def devices_reader(request):
    queryset = Device.objects.all().filter(is_active=True, device_type__name__icontains="card reader")
    serializer = DeviceSerializer(queryset, many=True)

    return HttpResponse(json.dumps(serializer.data), content_type="application/json")

def devices_btn(request):
    queryset = Device.objects.all().filter(is_active=True, device_type__name__icontains="button")
    serializer = DeviceSerializer(queryset, many=True)

    return HttpResponse(json.dumps(serializer.data), content_type="application/json")

def devices_door(request):
    queryset = Device.objects.all().filter(is_active=True, device_type__name__icontains="door")
    serializer = DeviceSerializer(queryset, many=True)
    return HttpResponse(json.dumps(serializer.data), content_type="application/json")

def devices_human(request):
    queryset = Device.objects.all().filter(is_active=True, device_type__name__icontains="human")
    serializer = DeviceSerializer(queryset, many=True)
    return HttpResponse(json.dumps(serializer.data), content_type="application/json")

def devices_led(request):
    queryset = Device.objects.all().filter(is_active=True, device_type__name__icontains="LED")
    serializer = DeviceSerializer(queryset, many=True)
    return HttpResponse(json.dumps(serializer.data), content_type="application/json")

class ManufacturerViewSet(viewsets.ViewSet):
    def create(self, request):
        data = request.data
        serializer = ManufacturerSerializer(data=data)
        if serializer.is_valid():
            manufacturer = serializer.save()

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        queryset = Manufacturer.objects.all().filter(is_active=True)
        object = get_object_or_404(queryset, pk=pk)
        object.updated_date = datetime.now()
        data = request.data
        serializer = ManufacturerSerializer(object, data=data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        queryset = Manufacturer.objects.all()
        object = get_object_or_404(queryset, pk=pk)
        object.is_active = False
        object.save()

        return Response()

    def list(self, request):
        queryset = Manufacturer.objects.all().filter(is_active=True)
        serializer = ManufacturerSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Manufacturer.objects.all().filter(is_active=True)
        manufacturer = get_object_or_404(queryset, pk=pk)
        serializer = ManufacturerSerializer(manufacturer)
        return Response(serializer.data)

class DeviceTypeViewSet(viewsets.ViewSet):

    def create(self, request):
        data = request.data
        manufacturer_instance = Manufacturer.objects.filter(id=data['manufacturer'], is_active=True).first()
        serializer = DeviceTypeSerializer(data=data)
        if serializer.is_valid():
            data = serializer.validated_data
            serializer.save(manufacturer=manufacturer_instance.id)

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        queryset = Device_type.objects.all().filter(is_active=True)
        object = get_object_or_404(queryset, pk=pk)
        object.updated_date = datetime.now()
        data = request.data
        serializer = DeviceTypeSerializer(object, data=data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        queryset = Device_type.objects.all()
        object = get_object_or_404(queryset, pk=pk)
        object.is_active = False
        object.save()

        return Response()

    def list(self, request):
        queryset = Device_type.objects.all().filter(is_active=True)
        serializer = DeviceTypeSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Device_type.objects.all().filter(is_active=True)
        deviceType = get_object_or_404(queryset, pk=pk)
        serializer = DeviceTypeSerializer(deviceType)
        return Response(serializer.data)

class DeviceViewSet(viewsets.ViewSet):
    def create(self, request):
        data = request.data
        device_type_instance = Device_type.objects.filter(id=data['device_type']).first()
        serializer = DeviceSerializer(data=data)
        if serializer.is_valid():
            device = serializer.save(device_type=device_type_instance.id)

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        queryset = Device.objects.all().filter(is_active=True)
        object = get_object_or_404(queryset, pk=pk)
        object.updated_date = datetime.now()
        current_connected = object.connected_devices
        data = json.loads(request.body)
        if 'connected_devices' in data:
	    print "connected_device!!!"
            if current_connected != None:
		print "BEFORE EMPTY"
                if data['connected_devices'] not in current_connected:
		    "NOT EXIST BEFORE"
                    current_connected = current_connected + ',' + str(data['connected_devices'])
            else:
		print "NOT EMPTY"
                current_connected = str(data['connected_devices'])
	    print "NOW CONNECTED "+current_connected
            data['connected_devices'] = current_connected

        serializer = DeviceSerializer(object, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        queryset = Device.objects.all()
        object = get_object_or_404(queryset, pk=pk)
        object.is_active = False
        object.delete()

        return Response()

    def list(self, request):
        queryset = Device.objects.all().filter(is_active=True)
        serializer = DeviceSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Device.objects.all().filter(is_active=True)
        device = get_object_or_404(queryset, pk=pk)
        serializer = DeviceSerializer(device)
        return Response(serializer.data)
