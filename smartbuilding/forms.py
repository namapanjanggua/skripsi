from django import forms
from models import Manufacturer, Device_type, Card

class ManufacturerForm(forms.ModelForm):

    class Meta:
        model = Manufacturer
        fields = ('name',)

class CardForm(forms.ModelForm):
    class Meta:
	model = Card
	fields = ('nama', 'kode_identitas',)

class DeviceTypeForm(forms.Form):
    manufacturers = Manufacturer.objects.all().filter(is_active=True)
    manufacturer_choices = []
    for manufacturer in manufacturers:
        manufacturer_choices.append([manufacturer.id, manufacturer.name])

    name = forms.CharField(max_length=160)
    manufacturer = forms.ChoiceField(choices=manufacturer_choices)

    value_types_choices = (
	('DIS', 'Discrete'),
	('CON', 'Continuous'),
    )
    value_types = forms.ChoiceField(choices=value_types_choices)

    category_choices = (
        (0, 'Sensor'),
	(1, 'Actuator'),
	(2, 'Resource'),
    )
    category = forms.ChoiceField(choices=category_choices)

class DeviceForm(forms.Form):
    def __init__(self, choices, *args, **kwargs):
        super(DeviceForm, self).__init__(*args, **kwargs)
        self.fields['name'] = forms.CharField(max_length=160)
        self.fields['port_gpio'] = forms.ChoiceField(choices=choices)
        deviceTypes = Device_type.objects.all().filter(is_active=True)
        device_type_choices = []
        for deviceType in deviceTypes:
            device_type_choices.append([deviceType.id, deviceType.name])

        self.fields['device_type'] = forms.ChoiceField(choices=device_type_choices)

class ReaderForm(forms.Form):
    def __init__(self, choices, *args, **kwargs):
        super(ReaderForm, self).__init__(*args, **kwargs)
        self.fields['name'] = forms.CharField(max_length=160)
	readers = []
	counter = 0
	for choice in choices:
	    print(choice.name)
	    readers.append([counter, choice.name])
	    counter = counter + 1
        self.fields['card_reader'] = forms.ChoiceField(choices=readers)
        deviceTypes = Device_type.objects.all().filter(name="Card Reader")
        device_type_choices = []
        for deviceType in deviceTypes:
            device_type_choices.append([deviceType.id, deviceType.name])
        self.fields['device_type'] = forms.ChoiceField(choices=device_type_choices)
