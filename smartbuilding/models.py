from django.db import models
from datetime import datetime

class Manufacturer(models.Model):
    name = models.CharField(max_length=160)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(
        default=datetime.now())
    updated_date = models.DateTimeField(
        blank=True, null=True)

class Device_type(models.Model):
    name = models.CharField(max_length=160)
    manufacturer = models.ForeignKey('manufacturer', on_delete=models.CASCADE, related_name="device_type")
    value_types = models.CharField(max_length=16, default='Discrete')
    category = models.CharField(max_length=160) # 0 is sensor 1 is actuator
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(
        default=datetime.now())
    updated_date = models.DateTimeField(
        blank=True, null=True)
    def __unicode__(self):
        return '%d: %s' % (self.order, self.title)

class Device(models.Model):
    name = models.CharField(max_length=160)
    port_gpio = models.IntegerField(default=None, blank=True, null=True)
    card_reader = models.TextField(default=None, blank=True, null=True)
    port_zmq = models.TextField(default=None, blank=True, null=True)
    pid_zmq = models.IntegerField(default=None, blank=True, null=True)
    device_type = models.ForeignKey('device_type', on_delete=models.CASCADE, related_name="device")
    pan_id = models.TextField()
    uid = models.TextField(default=None, blank=True, null=True)
    connected_devices = models.TextField(default=None, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(
        default=datetime.now())
    updated_date = models.DateTimeField(
        blank=True, null=True)

class GPIODevice(models.Model):
    port = models.IntegerField()
    deviceID = models.IntegerField()

class Card(models.Model):
    kode_identitas = models.CharField(max_length=160)
    nama = models.CharField(max_length=160)
    kode_organisasi = models.CharField(max_length=160, default=None, blank=True, null=True)
    created_date = models.DateTimeField(default=datetime.now(), blank=True, null=True)
    updated_date = models.DateTimeField(blank=True, null=True)
