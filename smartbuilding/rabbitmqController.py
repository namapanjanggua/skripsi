from models import Device
from utils import getPANAddress
from serializers import DeviceSerializer
import pika, json

def connectToMain():
    connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
    channel = connection.channel()
    return channel

def sendAction(body):
    body = json.loads(body)
    channel = connectToMain()
    message = {}
    message['body'] = body
    message['message_type'] = 'send_scenario'
    message = json.dumps(message)
    channel.basic_publish(exchange='',
    			  routing_key='broadcast',
    			  body=message)

def reportNewDevice(id):
    queryset = Device.objects.all().filter(is_active=True, id=id)
    serializer = DeviceSerializer(queryset, many=True)
    body = {}
    body['message_type'] = "broadcast_device"
    body['message'] = serializer.data
    body = json.dumps(body)
    channel = connectToMain()
    channel.basic_publish(exchange='',
                           routing_key='broadcast',
                           body=body)

def reportRoom():
    print("Reporting Room")
    body = {}
    body['message_type'] = "broadcast_room"
    body['pan_id'] = getPANAddress()
    body = json.dumps(body)
    channel = connectToMain()
    channel.basic_publish(exchange='',
			   routing_key='broadcast',
			   body=body)
    print("Done..")
