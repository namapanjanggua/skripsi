import pika,zmq, json, redis
import socket, netifaces, hashlib
from multiprocessing import Process

class RedisController(object):
    __instance = None

    @staticmethod
    def getInstance():
        if RedisController.__instance == None:
            RedisController()
        return RedisController.__instance

    def __init__(self):
        if RedisController.__instance == None:
            self.r = redis.StrictRedis(host="localhost", port=6379)
            RedisController.__instance = self

    def putKey(self, key, object):
        self.r.set(key, object)

    def putNewDevice(self, device, object):
        print "uid "+device['uid']
        self.r.setex("device_"+device['uid'], 300, object)

    def putNewRoom(self, pan_id, object):
        self.r.setex("room_"+pan_id, 300, object)


class RabbitMQController(object):
    __instance = None

    @staticmethod
    def getInstance():
        if RabbitMQController.__instance == None:
            RabbitMQController()
        return RabbitMQController.__instance

    def __init__(self):
        if RabbitMQController.__instance == None:
            self.credentials = pika.PlainCredentials('server1', 'password1')
            self.connection_outside = pika.BlockingConnection(pika.ConnectionParameters('152.118.148.103',
									20901,
									'vhost1',
									self.credentials,
									heartbeat_interval=0,
									blocked_connection_timeout=0))
            self.channel = self.connection_outside.channel()
            self.pan_id = getPANAddress()
            RabbitMQController.__instance = self
	    print "Connected!"

    def reinit(self):
        print("reinit..")
        try:
            self.connection_outside = pika.BlockingConnection(pika.ConnectionParameters('152.118.148.103',
                                                                        20901,
                                                                        'vhost1',
                                                                        self.credentials,
                                                                        heartbeat_interval=0,
                                                                        blocked_connection_timeout=0))
            self.channel = self.connection_outside.channel()
            print("reinit success..")
        except Exception as e:
            print(repr(e))


    def start(self):
        self.channel.queue_declare(queue="to."+self.pan_id)
        self.channel.queue_bind(exchange="exchange",
                       queue="to."+self.pan_id,
                       routing_key="room."+self.pan_id)
        self.channel.queue_bind(exchange="exchange",
                       queue="to."+self.pan_id,
                       routing_key="fanout")
        self.channel.basic_consume(callback_outside,
                          queue="to."+self.pan_id,
                          no_ack=True)
        print(" [x] Waiting for messages from outside. To exit press CTRL+C")
        self.channel.start_consuming()

    def publish(self, exchange_name, routing_key, message):
        try:
            self.channel.basic_publish(exchange=exchange_name,
                          routing_key=routing_key,
                          body=message)
        except:
            self.reinit()
            self.channel.basic_publish(exchange=exchange_name,
                          routing_key=routing_key,
                          body=message)

def getMacAddress():
    return netifaces.ifaddresses('eth0')[netifaces.AF_LINK][0]['addr']

def getPANAddress():
    mac = getMacAddress()
    encryptor = hashlib.sha1()
    encryptor.update(mac)
    return encryptor.hexdigest()

def callback_outside(ch, method, properties, body):
    print(" [x] Received from outside"+body)
    body = json.loads(body)
    type = body['message_type']
    if type == "broadcast_room":
        try:
            body_content = body['body']
            faculty = body_content['faculty']
            building = body_content['building']
            room = body_content['room']
            if room['pan_id'] != getPANAddress():
                new_body = {}
                new_body['faculty'] = faculty
                new_body['building'] = building
                new_body['room'] = room
                new_body = json.dumps(new_body)
                r = RedisController.getInstance()
                room_key = "room_"+room['pan_id']
                r.putNewRoom(room_key, new_body)
            else:
                r = redis.StrictRedis(host="localhost", port=6379)
                r.set('current_room', json.dumps(room))
                r.set('current_building', json.dumps(building))
                r.set('current_faculty', json.dumps(faculty))
        except Exception as e:
            print("Exception "+repr(e))
    elif type == "broadcast_device":
        try:
            body_content = body['body']
            device = body_content['device']
            body_content = json.dumps(body_content)
            r = RedisController.getInstance()
            r.putNewDevice(device, body_content)
            print("device saved!")
        except Exception as e:
            print("Exception "+repr(e))
    elif type == "send_scenario":
        try:
            device_from = body['body']['from']
            device_to = body['body']['to']
            code = body['body']['code']
            code = code.replace('\"','')
            code = code.replace('+','')
            current_dir_actuator = 'current/'+device_to['uid_device']
            current_file_actuator = current_dir_actuator+'/actuator_action_'+device_from['uid_device']+'.py'
            f = open(current_file_actuator, 'w')
            f.write(code)
            f.close()
            print("Success add scenario from "+device_from['uid_device']+"!")
        except Exception as e:
            print("Exception "+repr(e))
    elif type == "event":
        try:
            r = redis.StrictRedis()
            device_from = body['body']['from']['uid_device']
            device_to = body['body']['to']['uid_device']
            device_from = json.loads(r.get('device_'+device_from))
            device_to = json.loads(r.get('device_'+device_to))
            new_body = {}
            body = {}
            body['sender'] = device_from['device']['uid']
            body['target'] = device_to['device']['uid']
            new_body['body'] = body
            new_body['message_type'] = 'receive_event'
            new_body = json.dumps(new_body)
            context = zmq.Context()
            publisher = context.socket(zmq.REQ)
            publisher.connect("tcp://127.0.0.1:5555")
            publisher.send(new_body)
            print("send!")
            message = publisher.recv()
            print("action received!")
        except Exception as e:
            print("Exception "+repr(e))

def callback(ch, method, properties, body):
    print(" [x] Received "+ str(body))
    body = json.loads(str(body))
    type = body['message_type']
    message = {}
    message['message_type'] = body['message_type']

    try:
        if type == 'broadcast_room':
            pan_id = body['pan_id']
            new_body = {}
            new_body['pan_id'] = body['pan_id']
            message['body'] = new_body
            exchange_name = "federation"
            routing_key = "building"
        elif type == "broadcast_device":
            print "broadcast device!"
            r = redis.StrictRedis(host="localhost", port=6379)
            new_body = {}
            new_body['device'] = body['body']
            new_body['faculty'] = json.loads(r.get("current_faculty"))
            new_body['building'] = json.loads(r.get("current_building"))
            new_body['room'] = json.loads(r.get("current_room"))
            message['body'] = new_body
            exchange_name = "federation"
            routing_key = "fanout"
        elif type == "send_scenario":
            print "send Scenario"
            r = redis.StrictRedis()
            old_to = body['body']['to']
            old_to = json.loads(r.get('device_'+old_to['uid']))
            old_from = body['body']['from']
            old_code = body['body']['code']
            device_to = {}
            device_to['uid_device'] = old_to['device']['uid']
            device_to['building_pan_id'] = old_to['building']['pan_id']
            device_to['room_pan_id'] = old_to['room']['pan_id']
            device_from = {}
            device_from['uid_device'] = old_from['uid']
            device_from['building_pan_id'] = json.loads(r.get('current_building'))['pan_id']
            device_from['room_pan_id'] = json.loads(r.get('current_room'))['pan_id']
            new_body = {}
            new_body['from'] = device_from
            new_body['to'] = device_to
            new_body['code'] = old_code
            message['body'] = new_body
            exchange_name = "federation"
            routing_key = "faculty"
        elif type == "event":
            print("event broh")
            r = redis.StrictRedis()
            new_body = {}
            old_to = body['body']['to'].split("-")[1]
            old_to = r.get('device_'+old_to)
            old_to = json.loads(old_to)
            old_from = body['body']['from']
            old_from = json.loads(r.get("device_"+old_from))
            device_to = {}
            device_to['uid_device'] = old_to['device']['uid']
            device_to['building_pan_id'] = old_to['building']['pan_id']
            device_to['room_pan_id'] = old_to['room']['pan_id']
            device_from = {}
            device_from["uid_device"] = old_from['device']['uid']
            device_from['building_pan_id'] = old_from['building']['pan_id']
            device_from['room_pan_id'] = old_from['room']['pan_id']
            new_body['to'] = device_to
            new_body['from'] = device_from
            message['body'] = new_body
            exchange_name = "federation"
            routing_key = "faculty"

        message = json.dumps(message)
        rabbitMQoutside = RabbitMQController.getInstance()
        rabbitMQoutside.publish(exchange_name, routing_key, message)

        print "Forwarded to outside! "+message

    except Exception as e:
        print('Exception '+repr(e))

def receive_rabbit():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare('broadcast')
    channel.basic_consume(callback,
                      queue='broadcast',
                      no_ack=True)
    print(" [x] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()

rabbitMQoutside = RabbitMQController()
p1 = Process(target=receive_rabbit)
p2 = Process(target=rabbitMQoutside.start)
p1.start()
p2.start()
