import requests, redis
import json, zmq
from time import sleep

from smartcard.scard import SCARD_CTL_CODE
from smartcard.util import *

# CLEAR LCD READER
CLEAR_LCD = [0xFF, 0x00, 0x60, 0x00, 0x00]

CONTROL_CODE = 3500

def strToHex(teks, baris):
    result = [0xFF, 0x00, 0x68]
    #append baris
    if baris == 1:
        temp_baris = 0
    else:
        temp_baris = 64
    result.append(temp_baris)
    # append length
    result.append(len(teks))
    #append teks
    for t in teks:
        result.append(ord(t))
    return result

### MESSAGE
MESSAGE_SILAHKAN        = strToHex("SILAHKAN TAP", 1)
MESSAGE_KARTU           = strToHex("KARTU ANDA", 2)
MESSAGE_BERHASIL        = strToHex("ANDA BERHASIL", 1)
MESSAGE_TAPIN           = strToHex("TAP-IN", 2)                   
MESSAGE_TAPOUT          = strToHex("TAP-OUT", 2)                   
MESSAGE_DATA_KOSONG     = strToHex("DATA KOSONG", 2)                   
MESSAGE_ERROR           = strToHex("ERROR", 1)                    
MESSAGE_WRONG_CARD_1    = strToHex("KARTU TIDAK", 1)       
MESSAGE_WRONG_CARD_2    = strToHex("DAPAT DIKENALI", 2)    
MESSAGE_PROCESSING_1    = strToHex("MOHON ANDA", 1)        
MESSAGE_PROCESSING_2    = strToHex("TUNGGU SEBENTAR",2)    
MESSAGE_CARD_ERROR_1    = strToHex("KARTU TIDAK", 1)       
MESSAGE_CARD_ERROR_2    = strToHex("DAPAT DIBACA", 2)      
MESSAGE_TAP_ERROR_1     = strToHex("ID ANDA", 1)            
MESSAGE_TAP_ERROR_2     = strToHex("BELUM TERDAFTAR", 2)    
MESSAGE_KONEKSI_ERORR   = strToHex("KONEKSI ERROR",1)         
MESSAGE_GAGAL           = strToHex("GAGAL",1)
MESSAGE_BELUM_TAPOUT    = strToHex("BELUM TAP-OUT", 2)
MESSAGE_ADA_DATA        = strToHex("ADA DATA", 2)
MESSAGE_ANDA_TIDAK      = strToHex("ANDA TIDAK", 1)
MESSAGE_TERDAFTAR       = strToHex("TERDAFTAR", 2)
MESSAGE_PUNYA_AKSES     = strToHex("PUNYA AKSES", 2)
MESSAGE_STATUS_ANDA     = strToHex("STATUS ANDA", 1)
MESSAGE_TIDAK_AKTIF     = strToHex("TIDAK AKTIF", 2)
MESSAGE_WELCOME         = strToHex("SELAMAT DATANG", 1)

def convert_nama(nama):
    if len(nama) > 16:
        namas = nama.split(" ")
        nama = namas[0] + " " + namas[1]
    return strToHex(nama, 2)


# STATUS RESPONSE
RESPONSE_BERHASIL     		= 'berhasil'
RESPONSE_GAGAL        		= 'gagal'
RESPONSE_TIDAK_TERDAFTAR    = 'tidak_terdaftar'
RESPONSE_STATUS_NON_AKTIF   = 'status_non_aktif'

# URL_TAPIN   = 'http://localhost:8000/webservice/tapin/'
# URL_TAPOUT  = 'https://parkir.cs.ui.ac.id/webservice/tapout/'
# URL_TAPIN   = 'https://parkir.cs.ui.ac.id/webservice/tapin/'


import socket
HOSTNAME = socket.gethostname()

if HOSTNAME == 'raspberrypi':

    # SET GPIO
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup (11, GPIO.OUT)
    GPIO.setup (12, GPIO.OUT)

    def buka_palang():
        GPIO.output(12, True)
        time.sleep(0.5)
        GPIO.output(12, False) 
else:
    def buka_palang():
        print "===> buka palang"

# general function
def send_control(connection, codes):
    for code in codes:
        connection.control(SCARD_CTL_CODE(CONTROL_CODE), code)
    sleep(0.1)


def convert_cuid(cuid):
	chars_to_remove = ['[',']',' ']            
	rcuid = cuid.translate(None, ''.join(chars_to_remove))
	result = rcuid.replace(",",":")

	return result

def send_to_server(URL, kode_identitas, nama, kode_org):
    print ('send_to_server')
    try:
        myData = {'kode_identitas' : kode_identitas, 'nama':nama, 'kode_org':kode_org}
        print ('params: ', myData)

        headers = {'content-type': 'application/json'}
        responses = requests.get(URL, data=json.dumps(myData), headers=headers)
        # print(responses.content)

        json_object = json.loads((responses.content).decode('utf-8'))
        response = json_object['msg']
        print ('response: ',response)

        if response == RESPONSE_BERHASIL or response == RESPONSE_GAGAL:
            return True, response
        elif response == RESPONSE_STATUS_NON_AKTIF:
            return False, response
        else:
            return False, response
    
    except Exception as e:
        print ("Exception: ",e.args)
        return False, "ERROR"


### PROCESS MSG###
def process_msg(connection, tipe, status_code, response):
    print "process msg"
    if status_code:
        if tipe == 'masuk':
            if response == RESPONSE_BERHASIL:
                print ('MASUK sukses tapin')
                send_control(connection, [CLEAR_LCD, MESSAGE_BERHASIL, MESSAGE_TAPIN])
                buka_palang()

            elif response == RESPONSE_GAGAL:
                print ('MASUK tolak tapin -> pernah tapin')
                send_control(connection, [CLEAR_LCD, MESSAGE_GAGAL, MESSAGE_BELUM_TAPOUT])
        elif tipe == 'keluar':
            if response == RESPONSE_BERHASIL:
                send_control(connection, [CLEAR_LCD, MESSAGE_BERHASIL, MESSAGE_TAPOUT])
                buka_palang()
                
            elif response == RESPONSE_GAGAL:
                send_control(connection, [CLEAR_LCD, MESSAGE_GAGAL, MESSAGE_DATA_KOSONG])
    else:
        if response == RESPONSE_STATUS_NON_AKTIF:
            print (' MASUK status non aktif')
            send_control(connection, [CLEAR_LCD, MESSAGE_STATUS_ANDA, MESSAGE_TIDAK_AKTIF])
        elif response == RESPONSE_TIDAK_TERDAFTAR:
            print (' MASUK kartu tidak terdaftar')
            send_control(connection, [CLEAR_LCD, MESSAGE_TAP_ERROR_1, MESSAGE_TAP_ERROR_2])
        else:
            print (' koneksi error.')
            send_control(connection, [CLEAR_LCD, MESSAGE_KONEKSI_ERORR])

def send_zmq(port, body):
    print("sending "+body+" to "+port)
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:"+str(port))
    socket.send(body)
    message = socket.recv()
    print("Received reply %s [ %s ]" % (body,message))

def check_kartu(nama, kode_identitas):
    r = redis.StrictRedis()
    keys = r.keys(pattern="CARD_*_*")
    allowed = False
    for key in keys:
        object = r.get(key)
        object = json.loads(object)
        nama_match = (nama == object['nama'])
        kode_identitas_match = (kode_identitas == object['kode_identitas'])
        if (nama_match and kode_identitas_match):
            allowed = True
    if allowed:
        return True
    return False


def show_msg_tap(connection, tipe, card, temp_card_uid, temp_kode_identitas, temp_nama, temp_kode_org, port):
    kode_identitas = toASCIIString(temp_kode_identitas)
    nama = toASCIIString(temp_nama)
    kode_org = toASCIIString(temp_kode_org)
    cuid = str(temp_card_uid)
    print ('string card uid : ' , cuid)
    print ('toASCIIString kode_identitas : ' , kode_identitas)
    print ('toASCIIString nama : ' , nama)
    print ('toASCIIString kode_org : ' , kode_org)

    str_cuid = convert_cuid(cuid)

    if check_kartu(nama, kode_identitas):
        send_control(connection, [CLEAR_LCD, MESSAGE_WELCOME, convert_nama(nama)])
	message = {}
        message['message_type'] = "read_card"
        body = {}
        body['kode_identitas'] = kode_identitas
        body['nama'] = nama
        body['kode_org'] = kode_org
        message["body"] = body
        message = json.dumps(message)
        send_zmq(port, message)
    else:
	send_control(connection, [CLEAR_LCD, MESSAGE_TAP_ERROR_1, MESSAGE_TAP_ERROR_2])

    sleep(3)
    send_control(connection, [CLEAR_LCD, MESSAGE_SILAHKAN, MESSAGE_KARTU])

    print ("finish")
    connection.disconnect()
