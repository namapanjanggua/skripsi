import RPi.GPIO as GPIO
import zmq, sys, time, pika, json, os, urllib2, threading
from subprocess import call

def send_to_rabbit(channel, body):
    channel.queue_declare(queue='main')
    channel.basic_publish(exchange='',
                          routing_key='main',
                          body=body)

def create_channel_rabbit():
    params = pika.ConnectionParameters("localhost")
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    return channel

def subscribe_to_zmq():
    print("subscribing to zmq")
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://127.0.0.1:5556")
    socket.setsockopt(zmq.SUBSCRIBE, '')
    return socket

def setup_gpio(channel):
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    GPIO.setup(channel, GPIO.OUT)

def send_zmq(port, body):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://127.0.0.1:"+str(port))
    print("Sending request to "+port)
    socket.send(body)

    message = socket.recv()
    print("Received reply %s [ %s ]" % (body,message))

def create_new_zmq(port):
    channel_rabbitmq = create_channel_rabbit()

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://127.0.0.1:"+str(port))

    print "ZeroMQ receiving messages on port "+str(port)
    return socket, channel_rabbitmq

def broadcast_existence(body):
    send_zmq("5555", body)
    threading.Timer(300, broadcast_existence, [body]).start()

def listen_from_zmq(socket, channel, port, port_gpio, subscriber):
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    poller.register(subscriber, zmq.POLLIN)

    socks = dict(poller.poll())
    if socket in socks and socks[socket] == zmq.POLLIN:
        message = socket.recv()
    elif subscriber in socks and socks[subscriber] == zmq.POLLIN:
        message = subscriber.recv()

    if message:
        print("Received request: %s" % message)
	print "ADA MESSAGE"
        try:
            body = json.loads(message)
            sender = body['body']['sender']
	    print "SENDER "+sender
            dir_path = os.path.dirname(os.path.realpath(__file__))
            current_file = dir_path+"/actuator_action_"+sender+".py"
            if os.path.exists(current_file):
		print "CALL LED"
                call(["python", current_file])
        except:
            print("Exception in parsing message")

        if socket in socks and socks[socket] == zmq.POLLIN:
            socket.send("Echo "+message)

def set_high(channel):
    GPIO.output(channel, GPIO.HIGH)

def set_low(channel):
    GPIO.output(channel, GPIO.LOW)

port = sys.argv[1]
port_gpio = sys.argv[2]
id = sys.argv[3]
socket,channel = create_new_zmq(port)
subscriber = subscribe_to_zmq()
channel = (int)(port_gpio)
setup_gpio(channel)
set_low(channel)
device_body = urllib2.urlopen('http://localhost:8000/devices/'+id+'/').read().decode()
body = {}
body['message_type'] = 'broadcast_device'
body['body'] = json.loads(device_body)
body = json.dumps(body)
broadcast_existence(body)
while True:
    listen_from_zmq(socket, channel, port, port_gpio, subscriber)
