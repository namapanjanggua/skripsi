import zmq, sys, time, pika, json, threading, urllib2, redis
from multiprocessing import Process, Queue

def send_to_rabbit(channel, body):
    channel.queue_declare(queue='broadcast')
    channel.basic_publish(exchange='',
			  routing_key='broadcast',
			  body=body)

def send_zmq(port, body):
    print("sendinglala "+body)
    body = json.loads(body)
    message_type = body['message_type']
    if message_type == 'event':
	print "event ehe"
        if "-" in port:
	    print "ada pala"
            sender = body['body']['sender']
            message = {}
            body = {}
            body['from'] = sender
            body['to'] = port
            message['body'] = body
            message['message_type'] = 'event'
            message = json.dumps(message)
            channel_rabbit = create_channel_rabbit()
            send_to_rabbit(channel_rabbit, message)
            print("Event forwarded to rabbit!")
        else:
            context = zmq.Context()
            socket = context.socket(zmq.REQ)
            socket.connect("tcp://127.0.0.1:"+str(port))
            new_body = {}
            new_body['sender'] = body['body']['sender']
            message = {}
            message['message_type'] = 'receive_event'
            message['body'] = body['body']
            socket.send(json.dumps(message))
            message = socket.recv()
            print("Received reply %s [ %s ]" % (body,message))
    else:
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://127.0.0.1:"+str(port))
        socket.send(json.dumps(body))
        message = socket.recv()
        print("Received reply %s [ %s ]" % (body,message))

def check_device_still_available(pan_id):
    if "-" in pan_id:
	pan_id = pan_id.split("-")[1]
        print "checking "+pan_id
        r = redis.StrictRedis()
        if r.get("device_"+pan_id) is None:
            return False
        return True
    return True

def subscribe_to_zmq():
    print("subscribing to zmq")
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://127.0.0.1:5556")
    socket.setsockopt(zmq.SUBSCRIBE, '')
    return socket

def create_channel_rabbit():
    params = pika.ConnectionParameters("localhost")
    connection = pika.BlockingConnection(params)
    channel = connection.channel()
    return channel

def create_new_zmq(port):
    channel_rabbitmq = create_channel_rabbit()

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://127.0.0.1:"+str(port))

    print "ZeroMQ receiving messages on port "+str(port)
    return socket, channel_rabbitmq

def broadcast_existence(body):
    send_zmq("5555", body)
    threading.Timer(300, broadcast_existence, [body]).start()

def listen_from_zmq(socket, channel, port, subscriber):
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    poller.register(subscriber, zmq.POLLIN)

    socks = dict(poller.poll())
    if socket in socks and socks[socket] == zmq.POLLIN:
        message = socket.recv()
    elif subscriber in socks and socks[subscriber] == zmq.POLLIN:
        message = subscriber.recv()

    if message:
        print("Received request: %s" % message)
        try:
            body = json.loads(message)
            target = str(body['body']['target'])
            new_body = {}
            new_body['sender'] = body['body']['sender']
            new_body = json.dumps(new_body)

            if "," in target:
                targets = target.split(",")
                q = Queue()
                for target in targets:
		    print "now creating "+target
                    if check_device_still_available(target):
                        p = Process(target=send_zmq, args=(target,message))
                        p.start()
            else:
                if check_device_still_available(target):
		    print "device still avail "+target
                    send_zmq(target, message)
		else:
		    print "device not available"
    	except:
            print("Exception in parsing message")

        if socket in socks and socks[socket] == zmq.POLLIN:
            socket.send("Echo "+message)

port = sys.argv[1]
id = sys.argv[2]
socket,channel = create_new_zmq(port)
subscriber = subscribe_to_zmq()
device_body = urllib2.urlopen('http://localhost:8000/devices/'+id+'/').read().decode()
body = {}
body['message_type'] = 'broadcast_device'
body['body'] = json.loads(device_body)
body = json.dumps(body)
broadcast_existence(body)
while True:
    listen_from_zmq(socket, channel, port, subscriber)
