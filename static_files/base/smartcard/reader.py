
SELECT_CARD_UID = [0xFF, 0xCA,  0x00, 0x00, 0x00]

# KONSORSIUM
SELECT_CARD_KONSORSIUM = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x3F, 0x00]
SELECT_DF_GET_CARD_TYPE = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x10, 0x01]
SELECT_EF_GET_CARD_TYPE = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x01, 0xFF]
GET_CARD_TYPE = [0x00, 0xB0, 0x00, 0x00, 0x02]
SELECT_EF_FIELD_LENGTH_MAP = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x01, 0x00]
GET_FIELD_LENGTH_MAP = [0x00, 0xB0, 0x00, 0x00, 0x33]   
SELECT_EF_FIELD_1_SD_10 = [0x00, 0xA4, 0x00, 0x00, 0x02, 0x01, 0x01]
GET_EF_FIELD_1_SD_10 = [0x00, 0xB0, 0x00, 0x00, 0xFF]


#UI
SELECT_CARD_APPLET_MAHASISWA = [0x00, 0xA4, 0x04, 0x00, 0x06, 0xF3, 0x60, 0x00, 0x00, 0x01, 0x01]
SELECT_MAHASISWA = [0x00, 0xA4, 0x04, 0x00, 0x06, 0xF3, 0x60, 0x00, 0x00, 0x01, 0x01] 
SELECT_KODE_IDENTITAS = [0x90, 0x02, 0x02, 0x00, 0x00] # membaca kode identitas
SELECT_NAMA = [0x90, 0x02, 0x01, 0x00, 0x00] # membaca nama
SELECT_ORG = [0x90, 0x08, 0x01, 0x00, 0x00] #membaca kode organisasi


# UI
def proses_card_ui(connection, card_uid):
    print "proses UI"
    tmp, sw1, sw2 = connection.transmit(SELECT_MAHASISWA)

    kode_identitas, sw1, sw2 = connection.transmit(SELECT_KODE_IDENTITAS)
    if sw1 != 0x90:
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]

    nama, sw1, sw2 = connection.transmit(SELECT_NAMA)
    kode_org, sw1, sw2 = connection.transmit(SELECT_ORG)

    connection.disconnect()
    return card_uid, kode_identitas, nama, kode_org


# KONSORSIUM
def length_map(arr_length):
    total_length = []
    total_length.append(51) # Field Length pasti 51 byte
    while len(arr_length) > 0:
        x = arr_length.pop(0) * 256
        y = arr_length.pop(0)
        z = x + y
        total_length.append(z)

    return total_length

def str_fieldmap_to_bit_fieldmap(arr_field_map):
    arr_fieldmap = []
    arr_fieldmap.append(1)  # Field MAP 
    for x in arr_field_map:
        tmp = format(x, "08b")
        for y in tmp:
            arr_fieldmap.append(int(y))
    return arr_fieldmap

def ambil_field_1_sd_10(connection, length_map, bit_fieldmap):
    # NAMA FIELD KONSORSIUM
    nama_field = [
        'field_map',
        'local_card_id',
        'kode_institusi',
        'nomor_identitas',
        'nama_lengkap',
        'kode_organisasi',
        'nama_sekolah_institusi',
        'nama_fakultas_kelas',
        'nama_prodi_peminatan',
        'peran',
        'nomor_telpon',
        'icao1',
        'icao2',
        'foto',
        'sidik_jari',
        'alamat',
        'issue_date',
        'expired_date',
        'email',
        'status',
        'reserved1',
        'reserved2',
        'reserved3',
        'reserved4',
        'reserved5',
    ]

    tmp0, sw1, sw2 = connection.transmit(SELECT_EF_FIELD_1_SD_10)
    if sw1 != 0x90 and (sw1 != 0x61 and sw2 != 0x1C):
        return 

    total_1_sd_10 = 0
    # print(bit_fieldmap)
    # print(length_map)
    for x in range(1,10):
        if bit_fieldmap[x] == 1:
            total_1_sd_10 = total_1_sd_10 + length_map[x]

    resp, sw1, sw2 = connection.transmit(GET_EF_FIELD_1_SD_10)
    if sw1 != 0x90 and (sw1 != 0x61 and sw2 != 0x1C):
        return 

    if len(resp) == 0:
        return 

    hasil = {}
    data_fetched = 0
    for x in range(1, 10):
        if bit_fieldmap[x] == 1:
            tmp_nama_field = nama_field[x]
            banyak_data = length_map[x]
            
            tmp = []
            for y in range(data_fetched, data_fetched + banyak_data):
                tmp.append(resp[y])

            hasil[tmp_nama_field] = tmp
            data_fetched = data_fetched + banyak_data


    identitas = {}
    identitas['name'] = hasil['nama_lengkap']
    identitas['npm'] = hasil['nomor_identitas']
    identitas['kode_org'] = hasil['kode_organisasi']
    identitas['kode_institusi'] = hasil['kode_institusi']

    return identitas

def proses_card_konsorsium(connection, card_uid):
    print "Proses Konsorsium"
    tmp, sw1, sw2 = connection.transmit(SELECT_DF_GET_CARD_TYPE)
    if sw1 != 0x90:
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]
    
    tmp, sw1, sw2 = connection.transmit(SELECT_EF_GET_CARD_TYPE)
    if sw1 != 0x90:
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]

    tmp, sw1, sw2 = connection.transmit(GET_CARD_TYPE)
    if sw1 != 0x90:
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]

    tmp, sw1, sw2 = connection.transmit(SELECT_EF_FIELD_LENGTH_MAP)
    if sw1 != 0x90 or ( sw1 != 0x61 and sw2 != 0x1C):
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]

    tmp2, sw1, sw2 = connection.transmit(GET_FIELD_LENGTH_MAP)
    if sw1 != 0x90 or ( sw1 != 0x61 and sw2 != 0x1C):
        kode_identitas = [0x42, 0x55, 0x4b, 0x41, 0x4e, 0x5f, 0x55, 0x49]

    fieldmap1 = tmp2.pop(0)
    fieldmap2 = tmp2.pop(0)
    fieldmap3 = tmp2.pop(0)

    arr_length_map = length_map(tmp2)
    arr_field_map = str_fieldmap_to_bit_fieldmap([fieldmap1, fieldmap2, fieldmap3])

    identitas_field_1_sd_10 = ambil_field_1_sd_10(connection, arr_length_map, arr_field_map)

    nama = identitas_field_1_sd_10['name']
    kode_identitas = identitas_field_1_sd_10['npm']
    kode_org = identitas_field_1_sd_10['kode_org']

    connection.disconnect()
    return card_uid, kode_identitas, nama, kode_org